package retornoDePedidos

type HeaderArquivo00 struct {
	TipoRegistro      string `json:"TipoRegistro"`
	CnpjDistribuidor  string `json:"CnpjDistribuidor"`
	DataProcessamento int32  `json:"DataProcessamento"`
	HoraProcessamento int32  `json:"HoraProcessamento"`
}
