package retornoDePedidos

type ItemPedido struct {
	TipoRegistro               string `json:"TipoRegistro"`
	CnpjCliente                string `json:"CnpjCliente"`
	NumeroPedido               string `json:"NumeroPedido"`
	SequenciaItem              int32  `json:"SequenciaItem"`
	CodigoProduto              int64  `json:"CodigoProduto"`
	QuantidadeAtendida         int32  `json:"QuantidadeAtendida"`
	PosicaoItemPedido          int32  `json:"PosicaoItemPedido"`
	CodigoMotivoNaoAtendimento int32  `json:"CodigoMotivoNaoAtendimento"`
}
