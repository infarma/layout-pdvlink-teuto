package retornoDePedidos

type HeaderPedido03 struct {
	TipoRegistro               string  `json:"TipoRegistro"`
	CnpjCliente                string  `json:"CnpjCliente"`
	NumeroPedido               string  `json:"NumeroPedido"`
	NumeroPedidoDistribuidor   string  `json:"NumeroPedidoDistribuidor"`
	DataGeracaoRetorno         int32   `json:"DataGeracaoRetorno"`
	HoraGeracaoRetorno         int32   `json:"HoraGeracaoRetorno"`
	ValorAtendido              float64 `json:"ValorAtendido"`
	PosicaoPedido              int32   `json:"PosicaoPedido"`
	CodigoMotivoNaoAtendimento int32   `json:"CodigoMotivoNaoAtendimento"`
}
