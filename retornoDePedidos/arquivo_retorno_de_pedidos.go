package retornoDePedidos

import (
	"fmt"
	"bitbucket.org/infarma/layout-pdvlink-teuto/utilTeuto"
	"time"
)

// Header do Arquivo (Identificação do Distribuidor)
func HeaderArquivo(numCnpjDistribuidor string, dataHoraProcessamento time.Time) string {
	cabecalhoArquivo := fmt.Sprint("00;")
	cabecalhoArquivo += fmt.Sprintf("%014s", numCnpjDistribuidor) + ";"
	cabecalhoArquivo += dataHoraProcessamento.Format("02012006") + ";"
	cabecalhoArquivo += dataHoraProcessamento.Format("150405")
	return cabecalhoArquivo
}

// Header do Pedido
func HeaderPedido(numCnpj string, codPedCli string, numPdv int32, datHoraGeracaoRetorno time.Time, valorAtendido float64, codRejCab string) string {
	cabecalhoPedido := fmt.Sprint("03;")
	cabecalhoPedido += fmt.Sprintf("%014s", numCnpj) + ";"
	cabecalhoPedido += fmt.Sprintf("%012s", codPedCli) + ";"
	cabecalhoPedido += fmt.Sprintf("%012d", numPdv) + ";"
	cabecalhoPedido += datHoraGeracaoRetorno.Format("02012006") + ";"
	cabecalhoPedido += datHoraGeracaoRetorno.Format("150405") + ";"
	cabecalhoPedido += fmt.Sprint(valorAtendido) + ";"
	cabecalhoPedido += fmt.Sprint("3;")

	todosLayouts := []string{"18", "21", "22", "23"}
	if utilTeuto.Contains(todosLayouts, codRejCab) {
		cabecalhoPedido += fmt.Sprint("16")
	} else {
		cabecalhoPedido += fmt.Sprint("09")
	}

	return cabecalhoPedido
}

// Itens do Pedido
func ItensPedido(numCnpj string, codPedCli string, numSeqPdc string, codPrdCli string, qtdAtendida int32, codRejCab string, codRejIte string) string {
	cabecalhoPedido := fmt.Sprint("04;")
	cabecalhoPedido += fmt.Sprintf("%014s", numCnpj) + ";"
	cabecalhoPedido += fmt.Sprintf("%012s", codPedCli) + ";"
	cabecalhoPedido += fmt.Sprintf("%05s", numSeqPdc) + ";"
	cabecalhoPedido += fmt.Sprintf("%013s", codPrdCli) + ";"
	cabecalhoPedido += fmt.Sprintf("%06d", qtdAtendida) + ";"
	cabecalhoPedido += fmt.Sprint("3;")

	var codMtvRej string
	if codRejIte != "" {
		codMtvRej = codRejIte
	} else if codRejCab != "" {
		codMtvRej = codRejCab
	}

	todosLayouts := []string{"18", "21", "22", "23"}
	if utilTeuto.Contains(todosLayouts, codMtvRej) {
		cabecalhoPedido += fmt.Sprint("18")
	} else {
		cabecalhoPedido += fmt.Sprint("")
	}

	return cabecalhoPedido
}
