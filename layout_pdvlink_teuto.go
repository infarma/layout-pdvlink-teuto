package layout_pdvlink_teuto

import (
	"bitbucket.org/infarma/layout-pdvlink-teuto/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}