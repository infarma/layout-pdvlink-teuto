package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pdvlink-teuto/utilTeuto"
	"strings"
)

type Cliente struct {
	TipoRegistro                   string  `json:"TipoRegistro"`
	CnpjCliente                    string  `json:"CnpjCliente"`
	Nome                           string  `json:"Nome"`
	NomeFantasia                   string  `json:"NomeFantasia"`
	Endereco                       string  `json:"Endereco"`
	Bairro                         string  `json:"Bairro"`
	Cidade                         string  `json:"Cidade"`
	Estado                         string  `json:"Estado"`
	Cep                            string  `json:"Cep"`
	Telefone                       string  `json:"Telefone"`
	Contato                        string  `json:"Contato"`
	CargoContato                   string  `json:"CargoContato"`
	Email                          string  `json:"Email"`
	DataCadastro                   string  `json:"DataCadastro"`
	SugestaoLimiteCreditoCadastral float64 `json:"SugestaoLimiteCreditoCadastral"`
	VencimentoLimiteCredito        string  `json:"VencimentoLimiteCredito"`
	InscricaoEstadual              string  `json:"InscricaoEstadual"`
	Suframa                        string  `json:"Suframa"`
	ObservacaoCliente              string  `json:"ObservacaoCliente"`
	ObservacaoNotaFiscalCliente    string  `json:"ObservacaoNotaFiscalCliente"`
}

func GetCliente(runes []rune) Cliente{
	vector := strings.Split(string(runes), ";")
	cliente := Cliente{}

	cliente.TipoRegistro = vector[0]
	cliente.CnpjCliente = vector[1]
	cliente.Nome = vector[2]
	cliente.NomeFantasia = vector[3]
	cliente.Endereco = vector[4]
	cliente.Bairro = vector[5]
	cliente.Cidade = vector[6]
	cliente.Estado = vector[7]
	cliente.Cep = vector[8]
	cliente.Telefone = vector[9]
	cliente.Contato = vector[10]
	cliente.CargoContato = vector[11]
	cliente.Email = vector[12]
	cliente.DataCadastro = vector[13]
	cliente.SugestaoLimiteCreditoCadastral = utilTeuto.ConvertStringToFloat(vector[14])
	cliente.VencimentoLimiteCredito = vector[15]
	cliente.InscricaoEstadual = vector[16]
	cliente.Suframa = vector[17]
	cliente.ObservacaoCliente = vector[18]
	cliente.ObservacaoNotaFiscalCliente = vector[19]

	return cliente
}
