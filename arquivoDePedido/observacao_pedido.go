package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pdvlink-teuto/utilTeuto"
	"strings"
)

type ObservacaoPedido struct {
	TipoRegistro          string `json:"TipoRegistro"`
	CnpjCliente           string `json:"CnpjCliente"`
	NumeroPedido          string `json:"NumeroPedido"`
	NumeroLinhaObservacao int32  `json:"NumeroLinhaObservacao"`
	MensagemPedido        string `json:"MensagemPedido"`
	ObservacaoNotaFiscal  string `json:"ObservacaoNotaFiscal"`
	Observacao            string `json:"Observacao"`
}

func GetObservacaoPedido(runes []rune) ObservacaoPedido{
	vector := strings.Split(string(runes), ";")
	observacaoPedido := ObservacaoPedido{}

	observacaoPedido.TipoRegistro = vector[0]
	observacaoPedido.CnpjCliente = vector[1]
	observacaoPedido.NumeroPedido = vector[2]
	observacaoPedido.NumeroLinhaObservacao = int32(utilTeuto.ConvertStringToInt(vector[3]))
	observacaoPedido.MensagemPedido = vector[4]
	observacaoPedido.ObservacaoNotaFiscal = vector[5]
	observacaoPedido.Observacao = vector[6]

	return observacaoPedido
}
