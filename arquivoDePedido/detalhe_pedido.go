package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pdvlink-teuto/utilTeuto"
	"strings"
)

type DetalhePedido struct {
	TipoRegistro      string  `json:"TipoRegistro"`
	CnpjCliente       string  `json:"CnpjCliente"`
	NumeroPedido      string  `json:"NumeroPedido"`
	SequenciaItem     int32   `json:"SequenciaItem"`
	CodigoProduto     int64   `json:"CodigoProduto"`
	Quantidade        int32   `json:"Quantidade"`
	Desconto          float32 `json:"Desconto"`
	ValorUnitarioItem float64 `json:"ValorUnitarioItem"`
	ValorTotalItem    float64 `json:"ValorTotalItem"`
	TipoItem          int32   `json:"TipoItem"`
}

func GetDetalhePedido(runes []rune) DetalhePedido{
	vector := strings.Split(string(runes), ";")
	detalhePedido := DetalhePedido{}

	detalhePedido.TipoRegistro = vector[0]
	detalhePedido.CnpjCliente = vector[1]
	detalhePedido.NumeroPedido = vector[2]
	detalhePedido.SequenciaItem = int32(utilTeuto.ConvertStringToInt(vector[3]))
	detalhePedido.CodigoProduto = int64(utilTeuto.ConvertStringToInt(vector[4]))
	detalhePedido.Quantidade = int32(utilTeuto.ConvertStringToInt(vector[5]))
	detalhePedido.Desconto = float32(utilTeuto.ConvertStringToFloat(vector[6]))
	detalhePedido.ValorUnitarioItem = utilTeuto.ConvertStringToFloat(vector[7])
	detalhePedido.ValorTotalItem = utilTeuto.ConvertStringToFloat(vector[8])
	detalhePedido.TipoItem =int32(utilTeuto.ConvertStringToInt(vector[9]))

	return detalhePedido
}
