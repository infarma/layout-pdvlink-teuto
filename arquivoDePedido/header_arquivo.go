package arquivoDePedido

import (
	"strings"
)

type HeaderArquivo struct {
	TipoRegistro      string `json:"TipoRegistro"`
	CnpjDistribuidor  string `json:"CnpjDistribuidor"`
	DataProcessamento string `json:"DataProcessamento"`
	HoraProcessamento string `json:"HoraProcessamento"`
}

func GetHeaderArquivo(runes []rune) HeaderArquivo{
	vector := strings.Split(string(runes), ";")
	headerArquivo := HeaderArquivo{}

	headerArquivo.TipoRegistro = vector[0]
	headerArquivo.CnpjDistribuidor = vector[1]
	headerArquivo.DataProcessamento = vector[2]
	headerArquivo.HoraProcessamento = vector[3]

	return headerArquivo
}
