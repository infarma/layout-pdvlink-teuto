package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	HeaderArquivo    HeaderArquivo    `json:"HeaderArquivo"`
	Cliente          Cliente          `json:"Cliente"`
	HeaderPedido     HeaderPedido     `json:"HeaderPedido"`
	DetalhePedido    []DetalhePedido  `json:"DetalhePedido"`
	ObservacaoPedido ObservacaoPedido `json:"ObservacaoPedido"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "00" {
			arquivo.HeaderArquivo = GetHeaderArquivo(runes)
		} else if identificador == "08" {
			arquivo.Cliente = GetCliente(runes)
		} else if identificador == "01" {
			arquivo.HeaderPedido = GetHeaderPedido(runes)
		} else if identificador == "02" {
			getalhePedido := DetalhePedido{}
			getalhePedido = GetDetalhePedido(runes)
			arquivo.DetalhePedido = append(arquivo.DetalhePedido, getalhePedido)
		} else if identificador == "10" {
			arquivo.ObservacaoPedido = GetObservacaoPedido(runes)
		}
	}
	return arquivo, err
}
