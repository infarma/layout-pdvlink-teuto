package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pdvlink-teuto/utilTeuto"
	"strings"
)

type HeaderPedido struct {
	TipoRegistro        string  `json:"TipoRegistro"`
	CnpjCliente         string  `json:"CnpjCliente"`
	NumeroPedido        string  `json:"NumeroPedido"`
	DataPedido          string   `json:"DataPedido"`
	PrazoPagamento      string  `json:"PrazoPagamento"`
	NumeroPedidoCliente string  `json:"NumeroPedidoCliente"`
	TotalPedido         float64 `json:"TotalPedido"`
}
func GetHeaderPedido(runes []rune) HeaderPedido{
	vector := strings.Split(string(runes), ";")
	headerPedido := HeaderPedido{}

	headerPedido.TipoRegistro = vector[0]
	headerPedido.CnpjCliente = vector[1]
	headerPedido.NumeroPedido = vector[2]
	headerPedido.DataPedido = vector[3]
	headerPedido.PrazoPagamento = vector[4]
	headerPedido.NumeroPedidoCliente = vector[5]
	headerPedido.TotalPedido = utilTeuto.ConvertStringToFloat(vector[6])

	return headerPedido
}