package arquivoDeEstoqueTest

import (
	"layout-pdvlink-teuto/arquivoDeEstoque"
	"testing"
	"time"
)

func TestHeaderArquivoEstoque(t *testing.T){
	numCnpjDistribuidor := "15156220000186"

	src := "2019-10-21T09:56:13.001Z"
	dataHoraProcessamento, _ := time.Parse(time.RFC3339, src)

	headerArquivoEstoque := arquivoDeEstoque.HeaderArquivoEstoque(numCnpjDistribuidor, dataHoraProcessamento)

	if len(headerArquivoEstoque) != 33 {
		t.Error("Header Arquivo Estoque não tem o tamanho adeguado")
	}else{
		if headerArquivoEstoque != "00;15156220000186;21102019;095613" {
			t.Error("Header Arquivo Estoque não é compativel")
		}
	}
}

func TestDetalheEstoque(t *testing.T){
	codEan := "7729157139970"
	qtdEstoque := 500

	detalheEstoque := arquivoDeEstoque.DetalheEstoque(codEan, int64(qtdEstoque))

	if len(detalheEstoque) != 20 {
		t.Error("Detalhe Estoque não tem o tamanho adeguado")
	}else{
		if detalheEstoque != "09;7729157139970;500" {
			t.Error("Detalhe Estoque não é compativel")
		}
	}
}