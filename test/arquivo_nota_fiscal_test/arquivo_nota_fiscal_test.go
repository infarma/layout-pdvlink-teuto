package arquivo_nota_fiscal_test

import (
	"layout-pdvlink-teuto/arquivoDeNotaFiscal"
	"testing"
	"time"
)

func TestHeaderArquivo(t *testing.T){
	numCnpjDistribuidor := "15156220000186"

	src := "2019-10-21T09:56:13.001Z"
	dataHoraProcessamento, _ := time.Parse(time.RFC3339, src)

	headerArquivo := arquivoDeNotaFiscal.HeaderArquivoNotaFiscal(numCnpjDistribuidor, dataHoraProcessamento)

	if len(headerArquivo) != 33 {
		t.Error("Header Arquivo não tem o tamanho adeguado")
	}else{
		if headerArquivo != "00;15156220000186;21102019;095613" {
			t.Error("Header Arquivo não é compativel")
		}
	}
}

func TestHeaderNotaFiscal01(t *testing.T){
	numCnpj := "22571680000136"
	numNotaFIscal := "123"
	serieNotaFiscal := "1"
	numPdv := "123"
	numPdidodistribuidor := "123"

	src := "2019-10-21T09:56:13.001Z"
	dataHoraFaturamento, _ := time.Parse(time.RFC3339, src)

	tipeNotaFiscal := 1
	totalFaturado := 59.45
	valorSt := 12.41
	valorICMS := 12.41
	baseSt := 12.41
	baseIcms := 12.41
	vaorSuframa := 12.41

	headerNotaFiscal01 := arquivoDeNotaFiscal.HeaderNotaFiscal01(numCnpj, numNotaFIscal, serieNotaFiscal, numPdv, numPdidodistribuidor, dataHoraFaturamento,
																															int32(tipeNotaFiscal), totalFaturado, valorSt, valorICMS, baseSt, baseIcms, vaorSuframa)

	if len(headerNotaFiscal01) != 85 {
		t.Error("Header Nota Fiscal 01 não tem o tamanho adeguado")
	}else{
		if headerNotaFiscal01 != "05;22571680000136;123;1;123;123;21102019;095613;1;59.45;12.41;12.41;12.41;12.41;12.41" {
			t.Error("Header Nota Fiscal 01 não é compativel")
		}
	}
}

func TestItensNotaFiscal01(t *testing.T){
	numCnpj := "22571680000136"
	numNotaFIscal := "123"
	serieNotaFiscal := "1"
	numPdv := "123"
	numPdidodistribuidor := "123"
	sequenciaItem := 123
	codProduto := "7729157139970"
	qtdFaturada := 5
	valorUnitario := 12.41
	valorTotal := 59.45
	valorSt := 12.41
	valorICMS := 12.41
	baseSt := 12.41
	baseIcms := 12.41
	vaorSuframa := 12.41
	loteMedicamento := "123"
	chaveAcesso := "123"

	itensNotaFiscal01 := arquivoDeNotaFiscal.ItensNotaFiscal01(numCnpj, numNotaFIscal, serieNotaFiscal, numPdv, numPdidodistribuidor, int64(sequenciaItem),
																																codProduto, int64(qtdFaturada), valorUnitario, valorTotal, baseSt, valorSt, baseIcms, valorICMS, vaorSuframa,
																																loteMedicamento, chaveAcesso)

	if len(itensNotaFiscal01) != 101 {
		t.Error("Itens Nota Fiscal 01 não tem o tamanho adeguado")
	}else{
		if itensNotaFiscal01 != "06;22571680000136;123;1;123;123;123;7729157139970;5;12.41;59.45;12.41;12.41;12.41;12.41;12.41;123;123" {
			t.Error("Itens Nota Fiscal 01 não é compativel")
		}
	}
}