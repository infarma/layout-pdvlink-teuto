package retornoDePedidosTest

import (
	"layout-pdvlink-teuto/retornoDePedidos"
	"testing"
	"time"
)

func TestHeaderArquivo(t *testing.T){
	numCnpjDistribuidor := "15156220000186"

	src := "2019-10-21T09:56:13.001Z"
	dataHoraProcessamento, _ := time.Parse(time.RFC3339, src)

	headerArquivo := retornoDePedidos.HeaderArquivo(numCnpjDistribuidor, dataHoraProcessamento)

	if len(headerArquivo) != 33 {
		t.Error("Header Arquivo não tem o tamanho adeguado")
	}else{
		if headerArquivo != "00;15156220000186;21102019;095613" {
			t.Error("Header Arquivo não é compativel")
		}
	}
}

func TestHeaderPedido(t *testing.T){
	numCnpj := "22571680000136"
	codPedCli := "123"
	numPdv := 123

	src := "2019-10-21T09:56:13.001Z"
	datHoraGeracaoRetorno, _ := time.Parse(time.RFC3339, src)

	valorAtendido := 23.56

	codRejCab := 22

	headerPedido := retornoDePedidos.HeaderPedido(numCnpj, codPedCli, int32(numPdv), datHoraGeracaoRetorno, valorAtendido, string(codRejCab))

	if len(headerPedido) != 70 {
		t.Error("Header Pedido não tem o tamanho adeguado")
	}else{
		if headerPedido != "03;22571680000136;000000000123;000000000123;21102019;095613;23.56;3;09" {
			t.Error("Header Pedido não é compativel")
		}
	}
}

func TestItensPedido(t *testing.T){
	numCnpj := "15156220000186"
	codPedCli := "123"
	numSeqPdc := "1"
	codPrdCli := "7729157139970"
	codRejCab := 2
	codRejIte := "22"
	qtdAtendida := 3

	itensPedido := retornoDePedidos.ItensPedido(numCnpj, codPedCli, numSeqPdc, codPrdCli, int32(qtdAtendida), string(codRejCab), codRejIte)

	if len(itensPedido) != 62 {
		t.Error("Itens Pedido não tem o tamanho adeguado")
	}else{
		if itensPedido != "04;15156220000186;000000000123;00001;7729157139970;000003;3;18" {
			t.Error("Itens Pedidonão é compativel")
		}
	}
}