package arquivoDePedidoTest

import (
	"os"
	"testing"
	"layout-pdvlink-teuto/arquivoDePedido"
)

func TestGetArquivoPedido(t *testing.T){
	f, err := os.Open("../fileTest/OLPED1234211020190956123456.txt")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var cabecalho arquivoDePedido.HeaderArquivo
	cabecalho.TipoRegistro = "00"
	cabecalho.CnpjDistribuidor = "15156220000186"
	cabecalho.DataProcessamento = "21102019"
	cabecalho.HoraProcessamento = "095600"

	if cabecalho.TipoRegistro != arq.HeaderArquivo.TipoRegistro {
		t.Error("Tipo do registro não é compativel")
	}
	if cabecalho.CnpjDistribuidor != arq.HeaderArquivo.CnpjDistribuidor {
		t.Error("Cnpj Distribuidor não é compativel")
	}
	if cabecalho.DataProcessamento != arq.HeaderArquivo.DataProcessamento {
		t.Error("Data Processamento não é compativel")
	}
	if cabecalho.HoraProcessamento != arq.HeaderArquivo.HoraProcessamento {
		t.Error("Hora Processamento não é compativel")
	}

	var cliente arquivoDePedido.Cliente
	cliente.TipoRegistro = "08"
  cliente.CnpjCliente = "22571680000136"
  cliente.Nome = "Infarma Sistemas LTDA"
  cliente.NomeFantasia = "Infarma Sistemas"
  cliente.Endereco = "Av. Washington Soares, 855"
  cliente.Bairro = "Patriolino Ribeiro"
  cliente.Cidade = "Fortaleza"
  cliente.Estado = "CE"
  cliente.Cep = "60811341"
  cliente.Telefone = "8540628030"
  cliente.Contato = "8540628030"
  cliente.CargoContato = "Comercial"
  cliente.Email = "infarma@infarma.com.br"
  cliente.DataCadastro = "01012018"
  cliente.SugestaoLimiteCreditoCadastral = 0.00
  cliente.VencimentoLimiteCredito = "01012021"
	cliente.InscricaoEstadual = "123"
	cliente.Suframa =  "123"
	cliente.ObservacaoCliente = "ex"
	cliente.ObservacaoNotaFiscalCliente = "ex"

	if cliente.TipoRegistro != arq.Cliente.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if cliente.CnpjCliente != arq.Cliente.CnpjCliente {
		t.Error("Cnpj Cliente não é compativel")
	}
	if cliente.Nome != arq.Cliente.Nome {
		t.Error("Nome não é compativel")
	}
	if cliente.NomeFantasia != arq.Cliente.NomeFantasia {
		t.Error("Nome Fantasia não é compativel")
	}
	if cliente.Endereco != arq.Cliente.Endereco {
		t.Error("Endereco não é compativel")
	}
	if cliente.Bairro != arq.Cliente.Bairro {
		t.Error("Bairro não é compativel")
	}
	if cliente.Cidade != arq.Cliente.Cidade {
		t.Error("Cidade não é compativel")
	}
	if cliente.Estado != arq.Cliente.Estado {
		t.Error("Estado não é compativel")
	}
	if cliente.Cep != arq.Cliente.Cep {
		t.Error("Cep não é compativel")
	}
	if cliente.Telefone != arq.Cliente.Telefone {
		t.Error("Telefone não é compativel")
	}
	if cliente.Contato != arq.Cliente.Contato {
		t.Error("Contato não é compativel")
	}
	if cliente.CargoContato != arq.Cliente.CargoContato {
		t.Error("Cargo Contato não é compativel")
	}
	if cliente.Email != arq.Cliente.Email {
		t.Error("Email não é compativel")
	}
	if cliente.DataCadastro != arq.Cliente.DataCadastro {
		t.Error("Data Cadastro não é compativel")
	}
	if cliente.SugestaoLimiteCreditoCadastral != arq.Cliente.SugestaoLimiteCreditoCadastral {
		t.Error("Sugestao Limite Credito Cadastral não é compativel")
	}
	if cliente.VencimentoLimiteCredito != arq.Cliente.VencimentoLimiteCredito {
		t.Error("Vencimento Limite Credito não é compativel")
	}
	if cliente.InscricaoEstadual != arq.Cliente.InscricaoEstadual {
		t.Error("Inscrica oEstadual não é compativel")
	}
	if cliente.Suframa != arq.Cliente.Suframa {
		t.Error("Suframa não é compativel")
	}
	if cliente.ObservacaoCliente != arq.Cliente.ObservacaoCliente {
		t.Error("Observacao Cliente não é compativel")
	}
	if cliente.ObservacaoNotaFiscalCliente != arq.Cliente.ObservacaoNotaFiscalCliente {
		t.Error("Observacao Nota Fiscal Cliente não é compativel")
	}

	var headerPedido arquivoDePedido.HeaderPedido
	headerPedido.TipoRegistro = "01"
	headerPedido.CnpjCliente = "22571680000136"
	headerPedido.NumeroPedido = "123"
	headerPedido.DataPedido = "21102019"
	headerPedido.PrazoPagamento = "30"
	headerPedido.NumeroPedidoCliente = "123"
	headerPedido.TotalPedido = 100.00

	if headerPedido.TipoRegistro != arq.HeaderPedido.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if headerPedido.CnpjCliente != arq.HeaderPedido.CnpjCliente {
		t.Error("Cnpj Cliente não é compativel")
	}
	if headerPedido.NumeroPedido != arq.HeaderPedido.NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if headerPedido.DataPedido != arq.HeaderPedido.DataPedido {
		t.Error("Data Pedido não é compativel")
	}
	if headerPedido.PrazoPagamento != arq.HeaderPedido.PrazoPagamento {
		t.Error("Prazo Pagamentonão é compativel")
	}
	if headerPedido.NumeroPedidoCliente != arq.HeaderPedido.NumeroPedidoCliente {
		t.Error("Numero Pedido Cliente não é compativel")
	}
	if headerPedido.TotalPedido != arq.HeaderPedido.TotalPedido {
		t.Error("Total Pedido não é compativel")
	}

	var detalhePedido arquivoDePedido.DetalhePedido
	detalhePedido.TipoRegistro = "02"
	detalhePedido.CnpjCliente = "22571680000136"
  detalhePedido.NumeroPedido = "123"
	detalhePedido.SequenciaItem = 123
	detalhePedido.CodigoProduto = 7729157139970
	detalhePedido.Quantidade = 50
	detalhePedido.Desconto = 2.00
	detalhePedido.ValorUnitarioItem = 10.00
	detalhePedido.ValorTotalItem = 60.00
	detalhePedido.TipoItem = 1

	if detalhePedido.TipoRegistro != arq.DetalhePedido[0].TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if detalhePedido.CnpjCliente != arq.DetalhePedido[0].CnpjCliente {
		t.Error("Cnpj Cliente não é compativel")
	}
	if detalhePedido.NumeroPedido != arq.DetalhePedido[0].NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if detalhePedido.SequenciaItem != arq.DetalhePedido[0].SequenciaItem {
		t.Error("Sequencia Item não é compativel")
	}
	if detalhePedido.CodigoProduto != arq.DetalhePedido[0].CodigoProduto {
		t.Error("Codigo Produto não é compativel")
	}
	if detalhePedido.Quantidade != arq.DetalhePedido[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}
	if detalhePedido.Desconto != arq.DetalhePedido[0].Desconto {
		t.Error("Desconto não é compativel")
	}
	if detalhePedido.ValorUnitarioItem != arq.DetalhePedido[0].ValorUnitarioItem {
		t.Error("Valor Unitario Item não é compativel")
	}
	if detalhePedido.ValorTotalItem != arq.DetalhePedido[0].ValorTotalItem {
		t.Error("Valor Total Item não é compativel")
	}
	if detalhePedido.TipoItem != arq.DetalhePedido[0].TipoItem {
		t.Error("Tipo Item não é compativel")
	}

	var observacaoPedido arquivoDePedido.ObservacaoPedido
	observacaoPedido.TipoRegistro = "10"
	observacaoPedido.CnpjCliente = "22571680000136"
	observacaoPedido.NumeroPedido = "123"
	observacaoPedido.NumeroLinhaObservacao = 1
	observacaoPedido.MensagemPedido = "N"
	observacaoPedido.ObservacaoNotaFiscal = "N"
	observacaoPedido.Observacao = ""

	if observacaoPedido.TipoRegistro != arq.ObservacaoPedido.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if observacaoPedido.CnpjCliente != arq.ObservacaoPedido.CnpjCliente {
		t.Error("Cnpj Cliente não é compativel")
	}
	if observacaoPedido.NumeroPedido != arq.ObservacaoPedido.NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if observacaoPedido.NumeroLinhaObservacao != arq.ObservacaoPedido.NumeroLinhaObservacao {
		t.Error("Numero Linha Observacao não é compativel")
	}
	if observacaoPedido.MensagemPedido != arq.ObservacaoPedido.MensagemPedido {
		t.Error("Mensagem Pedido não é compativel")
	}
	if observacaoPedido.ObservacaoNotaFiscal != arq.ObservacaoPedido.ObservacaoNotaFiscal {
		t.Error("Observacao Nota Fiscal não é compativel")
	}
	if observacaoPedido.Observacao != arq.ObservacaoPedido.Observacao {
		t.Error("Observacao não é compativel")
	}

}