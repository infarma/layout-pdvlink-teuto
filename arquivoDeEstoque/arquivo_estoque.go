package arquivoDeEstoque

import (
	"fmt"
	"time"
)

func HeaderArquivoEstoque(numCnpjDistribuidor string, dataHoraProcessamento time.Time) string {

	cabecalhoArquivo := fmt.Sprint("00;")
	cabecalhoArquivo += fmt.Sprintf("%14s", numCnpjDistribuidor) + ";"
	cabecalhoArquivo += dataHoraProcessamento.Format("02012006") + ";"
	cabecalhoArquivo += dataHoraProcessamento.Format("150405")

	return cabecalhoArquivo
}

func DetalheEstoque(codEan string, qtdEstoque int64) string {

	detalheEstoque := fmt.Sprint("09;")
	detalheEstoque += fmt.Sprint(codEan) + ";"
	detalheEstoque += fmt.Sprint(qtdEstoque)

	return detalheEstoque
}