package arquivoDeEstoque

type Estoque struct {
	TipoRegistro        string `json:"TipoRegistro"`
	CodigoBarrasProduto int64  `json:"CodigoBarrasProduto"`
	QantidadeEstoque    int32  `json:"QantidadeEstoque"`
}
