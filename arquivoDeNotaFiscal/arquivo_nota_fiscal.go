package arquivoDeNotaFiscal

import (
	"fmt"
	"time"
)

// Header do Arquivo (Identificação do Distribuidor)
func HeaderArquivoNotaFiscal(numCnpjDistribuidor string, dataHoraProcessamento time.Time) string {

	cabecalhoArquivo := fmt.Sprint("00;")
	cabecalhoArquivo += fmt.Sprintf("%14s", numCnpjDistribuidor) + ";"
	cabecalhoArquivo += dataHoraProcessamento.Format("02012006") + ";"
	cabecalhoArquivo += dataHoraProcessamento.Format("150405")

	return cabecalhoArquivo
}

// Header do Arquivo (Identificação do Distribuidor)
func HeaderNotaFiscal01(numCnpj string, numNotaFIscal string, serieNotaFiscal string, numPdv string, numPdidodistribuidor string, dataHoraFaturamento time.Time,
	                    tipeNotaFiscal int32, totalFaturado  float64, valorSt float64, valorICMS float64, baseSt float64, baseIcms float64, vaorSuframa float64) string {

	cabecalhoNotaFiscal := fmt.Sprint("05;")
	cabecalhoNotaFiscal += fmt.Sprint( numCnpj) + ";"
	cabecalhoNotaFiscal += fmt.Sprint( numNotaFIscal) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(serieNotaFiscal) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(numPdv) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(numPdidodistribuidor) + ";"
	cabecalhoNotaFiscal += dataHoraFaturamento.Format("02012006") + ";"
	cabecalhoNotaFiscal += dataHoraFaturamento.Format("150405") + ";"
	cabecalhoNotaFiscal += fmt.Sprint(tipeNotaFiscal) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(totalFaturado) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(valorSt) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(valorICMS) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(baseSt) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(baseIcms) + ";"
	cabecalhoNotaFiscal += fmt.Sprint(vaorSuframa)

	return cabecalhoNotaFiscal
}

// Header do Arquivo (Identificação do Distribuidor)
func ItensNotaFiscal01(numCnpj string, numNotaFIscal string, serieNotaFiscal string, numPdv string, numPdidodistribuidor string, sequenciaItem int64,
	codProduto string, qtdFaturada int64, valorUnitario float64, valorTotal float64, baseSt float64, valorSt float64, baseIcms float64, valorICMS float64, vaorSuframa float64,
	loteMedicamento string, chaveAcesso string) string {

	itensNotaFiscal01 := fmt.Sprint("06;")
	itensNotaFiscal01 += fmt.Sprint( numCnpj) + ";"
	itensNotaFiscal01 += fmt.Sprint( numNotaFIscal) + ";"
	itensNotaFiscal01 += fmt.Sprint(serieNotaFiscal) + ";"
	itensNotaFiscal01 += fmt.Sprint(numPdv) + ";"
	itensNotaFiscal01 += fmt.Sprint(numPdidodistribuidor) + ";"
	itensNotaFiscal01 += fmt.Sprint(sequenciaItem) + ";"
	itensNotaFiscal01 += fmt.Sprint(codProduto) + ";"
	itensNotaFiscal01 += fmt.Sprint(qtdFaturada) + ";"
	itensNotaFiscal01 += fmt.Sprint(valorUnitario) + ";"
	itensNotaFiscal01 += fmt.Sprint(valorTotal) + ";"
	itensNotaFiscal01 += fmt.Sprint(baseSt) + ";"
	itensNotaFiscal01 += fmt.Sprint(valorSt) + ";"
	itensNotaFiscal01 += fmt.Sprint(baseIcms) + ";"
	itensNotaFiscal01 += fmt.Sprint(valorICMS) + ";"
	itensNotaFiscal01 += fmt.Sprint(vaorSuframa) + ";"
	itensNotaFiscal01 += fmt.Sprint(loteMedicamento) + ";"
	itensNotaFiscal01 += fmt.Sprint(chaveAcesso)

	return itensNotaFiscal01
}

