package arquivoDeNotaFiscal

type ItensNotaFiscal struct {
	TipoRegistro             string  `json:"TipoRegistro"`
	CnpjCliente              string  `json:"CnpjCliente"`
	NumeroNotaFiscal         string  `json:"NumeroNotaFiscal"`
	SerieNotaFiscal          string  `json:"SerieNotaFiscal"`
	NumeroPedido             string  `json:"NumeroPedido"`
	NumeroPedidoDistribuidor string  `json:"NumeroPedidoDistribuidor"`
	SequenciaItem            int32   `json:"SequenciaItem"`
	CodigoProduto            int64   `json:"CodigoProduto"`
	QunatidadeFaturada       int32   `json:"QunatidadeFaturada"`
	ValorUnitarioItem        float64 `json:"ValorUnitarioItem"`
	ValorTotalItem           float64 `json:"ValorTotalItem"`
	BaseST                   float64 `json:"BaseST"`
	ValorST                  float64 `json:"ValorST"`
	BaseICMS                 float64 `json:"BaseICMS"`
	ValorSuframa             float64 `json:"ValorSuframa"`
	LoteMedicamento          string  `json:"LoteMedicamento"`
	ChaveAcesso              string  `json:"ChaveAcesso"`
}
