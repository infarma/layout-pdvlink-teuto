package arquivoDeNotaFiscal

type HeaderNotaFiscal struct {
	TipoRegistro             string  `json:"TipoRegistro"`
	CnpjCliente              string  `json:"CnpjCliente"`
	NumeroNotaFiscal         string  `json:"NumeroNotaFiscal"`
	SerieNotaFiscal          string  `json:"SerieNotaFiscal"`
	NumeroPedido             string  `json:"NumeroPedido"`
	NumeroPedidoDistribuidor string  `json:"NumeroPedidoDistribuidor"`
	DataFaturamento          int32   `json:"DataFaturamento"`
	HoraFaturamento          int32   `json:"HoraFaturamento"`
	TipoNotaFiscal           int32   `json:"TipoNotaFiscal"`
	TotalFaturado            float64 `json:"TotalFaturado"`
	ValorST                  float64 `json:"ValorST"`
	ValorICMS                float64 `json:"ValorICMS"`
	BaseST                   float64 `json:"BaseST"`
	BaseICMS                 float64 `json:"BaseICMS"`
	ValorSuframa             float64 `json:"ValorSuframa"`
}
