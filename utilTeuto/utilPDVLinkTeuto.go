package utilTeuto

import (
	"strconv"
	"strings"
)

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(strings.TrimSpace(valor))
	return v
}
func ConvertStringToFloat(valor string) float64 {
	v, _ := strconv.ParseFloat(strings.TrimSpace(valor), 10)
	return v
}

func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}