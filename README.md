## Arquivo de Pedido
gerador-layouts arquivoDePedido headerArquivo TipoRegistro:string:0:2 CnpjDistribuidor:string:2:16 DataProcessamento:int32:16:24 HoraProcessamento:int32:24:32

gerador-layouts arquivoDePedido cliente TipoRegistro:string:0:2 CnpjCliente:string:2:16 Nome:string:16:76 NomeFantasia:string:76:106 Endereco:string:106:166 Bairro:string:166:226 Cidade:string:226:286 Estado:string:286:288 Cep:string:288:297 Telefone:string:297:357 Contato:string:357:407 CargoContato:string:407:457 Email:string:457:557 DataCadastro:int32:557:565 SugestaoLimiteCreditoCadastral:float64:565:577:2 VencimentoLimiteCredito:int32:577:585 InscricaoEstadual:string:585:605 Sufarma:string:605:625 ObservacaoCliente:string:625:1125 ObservacaoNotaFiscalCliente:string:1125:1380

gerador-layouts arquivoDePedido headerPedido TipoRegistro:string:0:2 CnpjCliente:string:2:16 NumeroPedido:string:16:28 DataPedido:int32:28:36 PrazoPagamento:string:36:96 NumeroPedidoCliente:string:96:116 TotalPedido:float64:116:128:2
 
gerador-layouts arquivoDePedido detalhePedido TipoRegistro:string:0:2 CnpjCliente:string:2:16 NumeroPedido:string:16:28 SequenciaItem:int32:28:33 CodigoProduto:int64:33:46 Quantidade:int32:46:52 Desconto:float32:52:57:2 ValorUnitarioItem:float64:57:69:2 ValorTotalItem:float64:69:81:2 TipoItem:int32:81:82

gerador-layouts arquivoDePedido observacaoPedido TipoRegistro:string:0:2 CnpjCliente:string:2:16 NumeroPedido:string:16:28 NumeroLinhaObservacao:int32:28:30 MensagemPedido:string:30:31 ObservacaoNotaFiscal:string:31:32 Observacao:string:32:132


## Arquivo de Retorno
gerador-layouts retornoDePedidos headerArquivo TipoRegistro:string:0:2 CnpjDistribuidor:string:2:16 DataProcessamento:int32:16:24 HoraProcessamento:int32:24:32

gerador-layouts retornoDePedidos headerPedido TipoRegistro:string:0:2 CnpjCliente:string:2:16 NumeroPedido:string:16:28 NumeroPedidoDistribuidor:sting:28:40 DataGeracaoRetorno:int32:40:48 HoraGeracaoRetorno:int32:48:56 ValorAtendido:float64:56:68:2 PosicaoPedido:int32:68:70 CodigoMotivoNaoAtendimento:int32:70:72

gerador-layouts retornoDePedidos itemPedido TipoRegistro:string:0:2 CnpjCliente:string:2:16 NumeroPedido:string:16:28 SequenciaItem:int32:28:33 CodigoProduto:int64:33:46 QuantidadeAtendida:int32:46:52 PosicaoItemPedido:int32:52:54 CodigoMotivoNaoAtendimento:int32:54:56


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal headerArquivo TipoRegistro:string:0:2 CnpjDistribuidor:string:2:16 DataProcessamento:int32:16:24 HoraProcessamento:int32:24:32

gerador-layouts arquivoDeNotaFiscal headerNotaFiscal TipoRegistro:string:0:2 CnpjCliente:string:2:16 NumeroNotaFiscal:string:16:26 SerieNotaFiscal:string:26:31 NumeroPedido:string:31:43 NumeroPedidoDistribuidor:string:43:55 DataFaturamento:int32:55:63 HoraFaturamento:int32:63:71 TipoNotaFiscal:int32:71:73 TotalFaturado:float64:73:85:2 ValorST:float64:85:97:2 ValorICMS:float64:97:109:2 BaseST:float64:109:121:2 BaseICMS:float64:121:133:2 ValorSuframa:float64:133:145:2

gerador-layouts arquivoDeNotaFiscal itensNotaFiscal TipoRegistro:string:0:2 CnpjCliente:string:2:16 NumeroNotaFiscal:string:16:26 SerieNotaFiscal:string:26:31 NumeroPedido:string:31:43 NumeroPedidoDistribuidor:string:43:55 SequenciaItem:int32:55:60 CodigoProduto:int64:60:73 QunatidadeFaturada:int32:73:79 ValorUnitarioItem:float64:79:91:2 ValorTotalItem:float64:91:103:2 BaseST:float64:103:115:2 ValorST:float64:115:127:2 BaseICMS:float64:127:139:2 ValorSuframa:float64:139:151:2 LoteMedicamento:string:151:171 ChaveAcesso:string:171:215


## Arquivo de Estoque
gerador-layouts arquivoDeEstoque headerArquivo TipoRegistro:string:0:2 CnpjDistribuidor:string:2:16 DataProcessamento:int32:16:24 HoraProcessamento:int32:24:32

gerador-layouts arquivoDeEstoque estoque TipoRegistro:string:0:2 CodigoBarrasProduto:int64:2:15 QantidadeEstoque:int32:15:21